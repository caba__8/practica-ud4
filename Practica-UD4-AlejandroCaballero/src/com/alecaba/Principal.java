package com.alecaba;

import com.alecaba.gui.Vista;
import com.alecaba.gui.Modelo;
import com.alecaba.gui.Controlador;

/**
 * Clase Principal
 * @author
 */
public class Principal {
    /**
     * Método main(), es estatico e inicia a la aplicacion
     * @param args de tipo String[]
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
