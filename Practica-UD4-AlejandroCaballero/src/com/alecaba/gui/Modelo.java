package com.alecaba.gui;

import com.alecaba.base.Tienda;
import com.alecaba.base.Cliente;
import com.alecaba.base.Producto;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Modelo {

    private MongoClient cliente;
    private MongoCollection<Document> productos;
    private MongoCollection<Document> clientes;
    private MongoCollection<Document> tiendas;

    public void conectar() {
        cliente = new MongoClient();
        String DATABASE = "Productos";
        MongoDatabase db = cliente.getDatabase(DATABASE);

        String COLECCION_PRODUCTOS = "Productos";
        productos = db.getCollection(COLECCION_PRODUCTOS);
        String COLECCION_CLIENTES = "Clientes";
        clientes = db.getCollection(COLECCION_CLIENTES);
        String COLECCION_TIENDAS = "Tiendas";
        tiendas = db.getCollection(COLECCION_TIENDAS);
    }

    public void desconectar() {
        cliente.close();
        cliente = null;
    }

    public MongoClient getCliente() {
        return cliente;
    }

    public ArrayList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<>();

        for (Document document : productos.find()) {
            lista.add(documentToProducto(document));
        }
        return lista;
    }

    public ArrayList<Producto> getProductos(String comparador) {
        ArrayList<Producto> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : productos.find(query)) {
            lista.add(documentToProducto(document));
        }

        return lista;
    }

    public ArrayList<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<>();

        for (Document document : clientes.find()) {
            lista.add(documentToCliente(document));
        }
        return lista;
    }

    public ArrayList<Cliente> getClientes(String comparador) {
        ArrayList<Cliente> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("apellidos", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : clientes.find(query)) {
            lista.add(documentToCliente(document));
        }

        return lista;
    }

    public ArrayList<Tienda> getTiendas() {
        ArrayList<Tienda> lista = new ArrayList<>();

        for (Document document : tiendas.find()) {
            lista.add(documentToTienda(document));
        }
        return lista;
    }

    public ArrayList<Tienda> getTiendas(String comparador) {
        ArrayList<Tienda> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("tienda", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : tiendas.find(query)) {
            lista.add(documentToTienda(document));
        }

        return lista;
    }

    public void guardarObjeto(Object obj) {
        if (obj instanceof Producto) {
            productos.insertOne(objectToDocument(obj));
        } else if (obj instanceof Cliente) {
            clientes.insertOne(objectToDocument(obj));
        } else if (obj instanceof Tienda) {
            tiendas.insertOne(objectToDocument(obj));
        }
    }

    public void modificarObjeto(Object obj) {
        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            productos.replaceOne(new Document("_id", producto.getId()), objectToDocument(producto));
        } else if (obj instanceof Cliente) {
            Cliente cliente = (Cliente) obj;
            clientes.replaceOne(new Document("_id", cliente.getId()), objectToDocument(cliente));
        } else if (obj instanceof Tienda) {
            Tienda tienda = (Tienda) obj;
            tiendas.replaceOne(new Document("_id", tienda.getId()), objectToDocument(tienda));
        }
    }

    public void eliminarObjeto(Object obj) {
        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            productos.deleteOne(objectToDocument(producto));
        } else if (obj instanceof Cliente) {
            Cliente cliente = (Cliente) obj;
            clientes.deleteOne(objectToDocument(cliente));
        } else if (obj instanceof Tienda) {
            Tienda tienda = (Tienda) obj;
            tiendas.deleteOne(objectToDocument(tienda));
        }
    }

    public Producto documentToProducto(Document dc) {
        Producto producto = new Producto();

        producto.setId(dc.getObjectId("_id"));
        producto.setNombre(dc.getString("nombre"));
        producto.setMarca(dc.getString("marca"));
        producto.setPrecio((Float.parseFloat(String.valueOf(dc.getDouble("precio")))));
        return producto;
    }

    public Cliente documentToCliente(Document dc) {
        Cliente cliente = new Cliente();

        cliente.setId(dc.getObjectId("_id"));
        cliente.setNombre(dc.getString("nombre"));
        cliente.setApellidos(dc.getString("apellidos"));
        cliente.setNacimiento(LocalDate.parse(dc.getString("nacimiento")));
        return cliente;
    }

    public Tienda documentToTienda(Document dc) {
        Tienda tienda = new Tienda();

        tienda.setId(dc.getObjectId("_id"));
        tienda.setTienda(dc.getString("tienda"));
        return tienda;
    }

    public Document objectToDocument(Object obj) {
        Document dc = new Document();

        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;

            dc.append("nombre", producto.getNombre());
            dc.append("marca", producto.getMarca());
            dc.append("precio", producto.getPrecio());
        } else if (obj instanceof Cliente) {
            Cliente cliente = (Cliente) obj;

            dc.append("nombre", cliente.getNombre());
            dc.append("apellidos", cliente.getApellidos());
            dc.append("nacimiento", cliente.getNacimiento().toString());

        } else if (obj instanceof Tienda) {
            Tienda tienda = (Tienda) obj;

            dc.append("tienda", tienda.getTienda());
        } else {
            return null;
        }
        return dc;
    }
}
