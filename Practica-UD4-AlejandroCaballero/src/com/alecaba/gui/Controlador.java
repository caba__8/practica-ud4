package com.alecaba.gui;

import com.alecaba.base.Tienda;
import com.alecaba.base.Cliente;
import com.alecaba.base.Producto;
import com.alecaba.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    private com.alecaba.gui.Modelo modelo;
    private com.alecaba.gui.Vista vista;

    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);

        try {
            modelo.conectar();
            vista.itemConectar.setText("Desconectar");
            vista.setTitle("Tienda Surf - <CONECTADO>");
            setBotonesActivados(true);
            listarProductos();
            listarClientes();
            listarTiendas();
        } catch (Exception ex) {
            Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
        }
    }

    private void addActionListeners(ActionListener listener){
        vista.btnAddProducto.addActionListener(listener);
        vista.btnModProducto.addActionListener(listener);
        vista.btnDelProducto.addActionListener(listener);
        vista.btnAddCliente.addActionListener(listener);
        vista.btnModCliente.addActionListener(listener);
        vista.btnDelCliente.addActionListener(listener);
        vista.btnAddTienda.addActionListener(listener);
        vista.btnModTienda.addActionListener(listener);
        vista.btnDelTienda.addActionListener(listener);

        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listProductos.addListSelectionListener(listener);
        vista.listClientes.addListSelectionListener(listener);
        vista.listTienda.addListSelectionListener(listener);
    }

    private void addKeyListeners(KeyListener listener){
        vista.txtBuscarProducto.addKeyListener(listener);
        vista.txtBuscarCliente.addKeyListener(listener);
        vista.txtBuscarTienda.addKeyListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "conexion":
                try {
                    if (modelo.getCliente() == null) {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        vista.setTitle("Tienda Surf - <CONECTADO>");
                        setBotonesActivados(true);
                        listarProductos();
                        listarClientes();
                        listarTiendas();
                    } else {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        vista.setTitle("Tienda Surf - <SIN CONEXION>");
                        setBotonesActivados(false);
                        vista.dlmProductos.clear();
                        vista.dlmClientes.clear();
                        vista.dlmTiendas.clear();
                        limpiarCamposProducto();
                        limpiarCamposClientes();
                        limpiarCamposTienda();
                    }
                } catch (Exception ex) {
                    Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
                }
                break;

            case "salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "addProducto":
                if (comprobarCamposProducto()) {
                    modelo.guardarObjeto(new Producto(vista.txtNombreProducto.getText(),
                            vista.txtMarcaProducto.getText(),
                            Float.parseFloat(vista.txtPrecioProducto.getText())));
                    limpiarCamposProducto();
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar el producto en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarProductos();
                break;

            case "modProducto":
                if (vista.listProductos.getSelectedValue() != null) {
                    if (comprobarCamposProducto()) {
                        Producto producto = vista.listProductos.getSelectedValue();
                        producto.setNombre(vista.txtNombreProducto.getText());
                        producto.setMarca(vista.txtMarcaProducto.getText());
                        producto.setPrecio(Float.parseFloat(vista.txtPrecioProducto.getText()));
                        modelo.modificarObjeto(producto);
                        limpiarCamposProducto();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el producto en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarProductos();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "delProducto":
                if (vista.listProductos.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listProductos.getSelectedValue());
                    listarProductos();
                    limpiarCamposProducto();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "addEmpleado":
                if (comprobarCamposCliente()) {
                    modelo.guardarObjeto(new Cliente(vista.txtNombreCliente.getText(),
                            vista.txtApellidosCliente.getText(),
                            vista.dateNacimientoCliente.getDate()));
                    limpiarCamposClientes();
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar el cliente en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarClientes();
                break;

            case "modEmpleado":
                if (vista.listClientes.getSelectedValue() != null) {
                    if (comprobarCamposCliente()) {
                        Cliente cliente = vista.listClientes.getSelectedValue();
                        cliente.setNombre(vista.txtNombreCliente.getText());
                        cliente.setApellidos(vista.txtApellidosCliente.getText());
                        cliente.setNacimiento(vista.dateNacimientoCliente.getDate());
                        modelo.modificarObjeto(cliente);
                        limpiarCamposClientes();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el cliente en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarClientes();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "delEmpleado":
                if (vista.listClientes.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listClientes.getSelectedValue());
                    listarClientes();
                    limpiarCamposClientes();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "addDepartamento":
                if (comprobarCamposTienda()) {
                    modelo.guardarObjeto(new Tienda(vista.txtTienda.getText()));
                    limpiarCamposTienda();
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar la tienda en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarTiendas();
                break;

            case "modDepartamento":
                if (vista.listTienda.getSelectedValue() != null) {
                    if (comprobarCamposTienda()) {
                        Tienda tienda = vista.listTienda.getSelectedValue();
                        tienda.setTienda(vista.txtTienda.getText());
                        modelo.modificarObjeto(tienda);
                        limpiarCamposTienda();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar la tienda en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarTiendas();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "delDepartamento":
                if (vista.listTienda.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listTienda.getSelectedValue());
                    listarTiendas();
                    limpiarCamposTienda();
                    break;
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarProducto) {
            listarProductosBusqueda(modelo.getProductos(vista.txtBuscarProducto.getText()));
            if (vista.txtBuscarProducto.getText().isEmpty()) {
                vista.dlmProductosBusqueda.clear();
            }
        } else if (e.getSource() == vista.txtBuscarCliente) {
            listarClientessBusqueda(modelo.getClientes(vista.txtBuscarCliente.getText()));
            if (vista.txtBuscarCliente.getText().isEmpty()) {
                vista.dlmClientesBusqueda.clear();
            }
        } else if (e.getSource() == vista.txtBuscarTienda) {
            listarTiendasBusqueda(modelo.getTiendas(vista.txtBuscarTienda.getText()));
            if (vista.txtBuscarTienda.getText().isEmpty()) {
                vista.dlmTiendasBusqueda.clear();
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == vista.listProductos) {
            if (vista.listProductos.getSelectedValue() != null) {
                Producto producto = vista.listProductos.getSelectedValue();
                vista.txtNombreProducto.setText(producto.getNombre());
                vista.txtMarcaProducto.setText(producto.getMarca());
                vista.txtPrecioProducto.setText(String.valueOf(producto.getPrecio()));
            }
        } else if (e.getSource() == vista.listClientes) {
            if (vista.listClientes.getSelectedValue() != null) {
                Cliente cliente = vista.listClientes.getSelectedValue();
                vista.txtNombreCliente.setText(cliente.getNombre());
                vista.txtApellidosCliente.setText(cliente.getApellidos());
                vista.dateNacimientoCliente.setDate(cliente.getNacimiento());
            }
        } else if (e.getSource() == vista.listTienda) {
            if (vista.listTienda.getSelectedValue() != null) {
                Tienda tienda = vista.listTienda.getSelectedValue();
                vista.txtTienda.setText(tienda.getTienda());
            }
        }
    }

    private boolean comprobarCamposProducto() {
        return !vista.txtNombreProducto.getText().isEmpty() &&
                !vista.txtMarcaProducto.getText().isEmpty() &&
                !vista.txtPrecioProducto.getText().isEmpty() &&
                comprobarFloat(vista.txtPrecioProducto.getText());
    }

    private boolean comprobarCamposCliente() {
        return !vista.txtNombreCliente.getText().isEmpty() &&
                !vista.txtApellidosCliente.getText().isEmpty() &&
                !vista.dateNacimientoCliente.getText().isEmpty();
    }

    private boolean comprobarCamposTienda() {
        return !vista.txtTienda.getText().isEmpty();
    }

    private void limpiarCamposProducto() {
        vista.txtNombreProducto.setText("");
        vista.txtMarcaProducto.setText("");
        vista.txtPrecioProducto.setText("");
        vista.txtBuscarProducto.setText("");
    }

    private void limpiarCamposClientes() {
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.dateNacimientoCliente.clear();
        vista.txtBuscarCliente.setText("");
    }

    private void limpiarCamposTienda() {
        vista.txtTienda.setText("");
        vista.txtBuscarTienda.setText("");
    }

    private boolean comprobarFloat(String txt) {
        try {
            Float.parseFloat(txt);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private void listarProductos() {
        vista.dlmProductos.clear();
        for (Producto producto : modelo.getProductos()) {
            vista.dlmProductos.addElement(producto);
        }
    }

    private void listarClientes() {
        vista.dlmClientes.clear();
        for (Cliente cliente : modelo.getClientes()) {
            vista.dlmClientes.addElement(cliente);
        }
    }

    private void listarTiendas() {
        vista.dlmTiendas.clear();
        for (Tienda tienda : modelo.getTiendas()) {
            vista.dlmTiendas.addElement(tienda);
        }
    }

    private void listarProductosBusqueda(ArrayList<Producto> lista) {
        vista.dlmProductosBusqueda.clear();
        for (Producto producto : lista) {
            vista.dlmProductosBusqueda.addElement(producto);
        }
    }

    private void listarClientessBusqueda(ArrayList<Cliente> lista) {
        vista.dlmClientesBusqueda.clear();
        for (Cliente cliente : lista) {
            vista.dlmClientesBusqueda.addElement(cliente);
        }
    }

    private void listarTiendasBusqueda(ArrayList<Tienda> lista) {
        vista.dlmTiendasBusqueda.clear();
        for (Tienda tienda : lista) {
            vista.dlmTiendasBusqueda.addElement(tienda);
        }
    }

    private void setBotonesActivados(boolean activados) {
        vista.btnAddProducto.setEnabled(activados);
        vista.btnModProducto.setEnabled(activados);
        vista.btnDelProducto.setEnabled(activados);
        vista.btnAddCliente.setEnabled(activados);
        vista.btnModCliente.setEnabled(activados);
        vista.btnDelCliente.setEnabled(activados);
        vista.btnAddTienda.setEnabled(activados);
        vista.btnModTienda.setEnabled(activados);
        vista.btnDelTienda.setEnabled(activados);
    }

    // Métodos innecesarios
    @Override
    public void keyTyped(KeyEvent e) {}
    @Override
    public void keyPressed(KeyEvent e) {}
}
