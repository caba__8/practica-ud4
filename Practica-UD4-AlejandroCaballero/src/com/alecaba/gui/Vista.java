package com.alecaba.gui;

import com.alecaba.base.Tienda;
import com.alecaba.base.Cliente;
import com.alecaba.base.Producto;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame {
    private JPanel panelPrincipal;

    // Productos
    JTextField txtNombreProducto;
    JTextField txtMarcaProducto;
    JTextField txtPrecioProducto;

    JList<Producto> listProductos;

    JButton btnAddProducto;
    JButton btnModProducto;
    JButton btnDelProducto;

    JTextField txtBuscarProducto;
    JList<Producto> listBusquedaProducto;

    // Clientes
    JTextField txtNombreCliente;
    JTextField txtApellidosCliente;
    DatePicker dateNacimientoCliente;

    JList<Cliente> listClientes;

    JButton btnAddCliente;
    JButton btnModCliente;
    JButton btnDelCliente;

    JTextField txtBuscarCliente;
    JList<Cliente> listBusquedaCliente;

    // Tiendas
    JTextField txtTienda;

    JList<Tienda> listTienda;

    JButton btnAddTienda;
    JButton btnModTienda;
    JButton btnDelTienda;

    JTextField txtBuscarTienda;
    JList<Tienda> listBusquedaTienda;


    // Modelos
    DefaultListModel<Producto> dlmProductos;
    DefaultListModel<Cliente> dlmClientes;
    DefaultListModel<Tienda> dlmTiendas;
    DefaultListModel<Producto> dlmProductosBusqueda;
    DefaultListModel<Cliente> dlmClientesBusqueda;
    DefaultListModel<Tienda> dlmTiendasBusqueda;

    // Menu
    JMenuItem itemConectar;
    JMenuItem itemSalir;

    public Vista() {
        setTitle("Tienda Surf - <SIN CONEXION>");
        setContentPane(panelPrincipal);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(800, 650));
        setResizable(false);
        pack();
        setVisible(true);

        inicializarModelos();
        inicializarMenu();
    }

    private void inicializarModelos() {
        dlmProductos = new DefaultListModel<>();
        listProductos.setModel(dlmProductos);
        dlmClientes = new DefaultListModel<>();
        listClientes.setModel(dlmClientes);
        dlmTiendas = new DefaultListModel<>();
        listTienda.setModel(dlmTiendas);
        dlmProductosBusqueda = new DefaultListModel<>();
        listBusquedaProducto.setModel(dlmProductosBusqueda);
        dlmClientesBusqueda = new DefaultListModel<>();
        listBusquedaCliente.setModel(dlmClientesBusqueda);
        dlmTiendasBusqueda = new DefaultListModel<>();
        listBusquedaTienda.setModel(dlmTiendasBusqueda);
    }

    private void inicializarMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuArchivo);

        setJMenuBar(menuBar);
    }
}
