package com.alecaba.base;

import org.bson.types.ObjectId;

public class Producto {
    private ObjectId id;
    private String nombre;
    private String marca;
    private float precio;

    public Producto(String nombre, String marca, float precio) {
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
    }

    public Producto () {

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return nombre + " - " + marca + " - " + precio + " €";
    }
}
