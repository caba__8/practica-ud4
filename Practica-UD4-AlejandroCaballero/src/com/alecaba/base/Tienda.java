package com.alecaba.base;

import org.bson.types.ObjectId;

public class Tienda {
    private ObjectId id;
    private String tienda;

    public Tienda(String tienda) {
        this.tienda = tienda;
    }

    public Tienda() {

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }

    @Override
    public String toString() {
        return tienda;
    }
}